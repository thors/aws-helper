# Security Improvements

## Reasoning

AWS credentials are fucking dangerous, and when using credentials stored in ~/.aws/credentials,
there is no additional layer of security like asking for a password or anything. The credentials
are always stored in the same path, and thez can usually be used from anywhere in the internet, 
they are not restricted to a certain company  network or so.

Also, on most personal laptops, the credentials are not well protected. Reading the credentials 
and forwarding it to a malicious third party as an http payload would be a really low-effort attack.

AWS does not support any cost cap on its account, either, even losing credentials with relatively 
low permissions can incur insane costs (e.g. write access to a single S3-bucket). Let alone credentials
with permission to raise ec2 instances, and a malicious attacker maxing out available ec2 instances
to e.g. run DOS attacks or simply to mine crypto-currencies.

All this together makes the theft of credentials a nightmare way too realistic for my peace of mind.

## Concept

Instead of using the plain-text credentials file, we use a gpg-encrypted file which is decrypted in-memory
on start of any of our tools.

## Usage
### Preparation

* gpg2 (or gpg) needs to be available on your system
* create a key for a new user (for our example we will use "aws@localhost") 
  * `gpg2 --generate-key`
    * Real name: `AWS Helper`
    * Email: `aws-helper@localhost`
  * You will be prompted for a pass-phrase, it´s up to your perceived threat-level if you 
    want to leave it empty. As long as aws-helper isn`t widely used, even with an empty phrase
    it will improve the safety against low-effort-attacks; however, that is security by obscurity,
    which I personally would not endorse.
* create a file ~/.aws/gpg
  * add an entry `user=aws@localhost` 
* You can now encrypt your ~/.aws/credentials file (and decrypt yo verify the setup)
  * `gpg --encrypt --output ~/.aws/credentials.gpg --recipient aws-helper@localhost ~/.aws/credentials`
  * `gpg --decrypt --output ~/.aws/credentials.copy ~/.aws/credentials.gpg`

If everything worked, you can get rid of all clear-text credential files. You`ll have to 
encrypt it again any time you modify the file, though.  