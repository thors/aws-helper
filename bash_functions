function aws_login() {
	local account_name=$1
	if [ -z ${account_name} ]; then
        ~/bin/aws_get_credentials
	else
        $(~/bin/aws_get_credentials -a $account_name)
    fi
}
