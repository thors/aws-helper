# aws-helper

A small collection of tools helping to manage multiple AWS accounts on linux. Started as an 
exercise at home to learn a bit more about boto3 and aws, it became a small but useful set of scripts
supporting me in my daily work.

## What the tools *don't* do:

* Send any data to anywhere other than your AWS accounts or your local computer
* Collect any statistics to somehow improve user-experience -- Write an email if you want something!

## What the tools do:

* rely on the ~/.aws/credentials (or ~/.aws/credentials.gpg and ~/.aws/gpg, read security.md)
file to access your AWS accounts (https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-files.html)
* gathers information from your accounts and store them in ~/.aws/aws-helper/*
  * VPCs, subnets
  * S3 buckets
* use and connect gathered information to answer common questions 
  * For given IP, provide account, region, vpc and subnet
  * For a given S3 bucket name, provide the account
  
## Recommended setup

* Checkout the repository
* Make sure /usr/bin/python3 points to a python3 installation
* Install requirements from requirements.txt
* Copy or merge  bash_functions to ~/.bash_functions and source it in your ~/.bashrc or ~/.bash_profile
* Create a folder ~/bin and add to PATH in your ~/.bashrc or ~/.bash_profile

E.g.: 

    . ~/.bash_functions 
    PATH=$PATH:$HOME/bin

* cp -r bin/* ~/bin

## The tools in bin

All tools assume a valid ~/.aws/credentials file with an account alias for each account. The format should basically be alike

    [my_fun_account]
    aws_access_key_id=AKYOUWISHBUTNOXAXAXA
    aws_secret_access_key=AnSwEr+is/sTill//NO+Not+/that///DuMbxxxx
    
    [my_other_account]
    aws_access_key_id=AKYOUWISHBUTNOZBZBZB
    aws_secret_access_key=AnSwEr+is/sTill//NO+Not+/that///DuMbyyyy

Most unexpected lines will simply be ignored. 
All tools should provide some short information via $TOOL --help

### aws_get_subnets

Collect information about all your subnets in your accounts. The collected information is stored in 

    ./aws/aws-helper/account/$ACCOUNT_ID/subnets_$REGION.[json|csv]
    ./aws/aws-helper/summary/subnets.csv

The information can be used either directly, or e.g. via aws_net-info to get all related information about a certain IP

### aws_get_vpcs

Collect information about all your VPCs in your accounts. The collected information is stored in 

    ./aws/aws-helper/account/$ACCOUNT_ID/vpcs_$REGION.[json|csv]
    ./aws/aws-helper/summary/vpcs.csv

### aws_net-info

For a given ip or hostname, provides information in which account, region, vpc and subnet the ip is located (assuming it is in one of the accounts owned by the user
and the subnet-information downloaded by aws_get_subnets is up to date)

### aws_get_s3_buckets

Go through all accounts and list all S3-buckets, including account name, region-restrictions, storage usage for standard- and infrequent-access-storage. Data is stored in 

    .aws/aws-helper/account/$ACCOUNT_ID/s3_buckets.[json|csv]
    .aws/aws-helper/summary/s3_buckets.csv

### aws_get_credentials / aws_login

aws_get_credentials prints the commands required to set the environment variables to use a certain account. Given the pseudo-credentials file, it would be

    # aws_get_credentials -a my_fun_account
    export AWS_ACCESS_KEY_ID=AKYOUWISHBUTNOXAXAXA
    export AWS_SECRET_ACCESS_KEY=AnSwEr+is/sTill//NO+Not+/that///DuMbxxxx

aws_login is a bash function wrapper to aws_get_credentials to set the environment variables without echoing them on the command line. I.e.

    # aws_login my_fun_account

will get the access-key and secret-key and set the environment variables accordingly. All subsequent aws cli commands will use these credentials.

### aws_get_clean_credentials

This file is generated using bin/aws_get_clean_credentials. The tool reads the existing ~/.aws/credential file,
logs into each available account, and tries to get information about account name, account id, etc. The generated 
file contains this information (account id as a comment for each account).
It should be safe afterwards to copy ~/.aws/aws-helper/credentials.clean to ~/.aws/credentials, but as always it's even saver
to keep a backup.

### aws_instances-info

Currently this is probably not useful for the average user. It was for me, therefore I implemented it. I might extend it for more generic use-cases.
This tool takes a list of ips or host-names and filters them by tags. E.g. if you have a list of hosts that need to have a tag "owner", the tool can go through 
that list and print all hosts that are missing this tag.



This file it generated using bin/aws_get_subnets. It contains a table of all subnets in all your accounts with account-id, vpc, name tag etc. 
You can use bin/net-info --ip aaa.bbb.ccc.ddd to query the related information for a given IP. 

## Motivation

I'm working with multiple AWS accounts and often need subnet-ids, vpc-ids and other information
at my fingertips. I need to find out for a given IP, in which subnet / region / vpc it runs.
I need create config files providing subnet, vpc, availability-zone and want to minimize my
input data. (When the subnet ID is known, vpc and availability-zone can be looked up from gathered
data. This data can be added to repositories as well, as it rarely changes.)

## Coding Style
I try to maintain some consistency, but the tools (and especially the libraries) evolved over time, they were not all cleanly designed
and planned. Sometimes I have a good idea for a new script but not the time to add equivalent behaviour to the older scripts as well,
therefore sometimes the behaviour is inconsistent. The scripts are IMO small enough to be still readable.

If you want to do some cleanup, feel free to do so, I'll consider to merge commits for that purpose if I like them.

However, I'll only merge cleanup, if it makes the code more readable for the occasional python coder. I'll not merge anything based on
best-practices or abstraction if I think it reduces the readability of the code.
