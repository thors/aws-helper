import os
import datetime
import json
import boto3
import re
import botocore

def process(message):
    http_response = {'statusConde': 404, 'body': ''}
    if 'bucket_name' not in message:
        http_response['statusConde'] = 422
        http_response['body'] = 'Error: Missing parameter bucket name'
        return response
    bucket_name = message['bucket_name']
    if bucket_name[:5] == "s3://":
        bucket_name = bucket_name[5:]
    location_arn_re = re.compile(".*:([^:]*)$")
    cli = boto3.client('storagegateway', region_name='us-east-1')
    filesharesarn = []
    cli_response = cli.list_file_shares()
    for fileshare in cli_response['FileShareInfoList']:
        filesharesarn.append(fileshare['FileShareARN'])

    shares = cli.describe_nfs_file_shares(FileShareARNList=filesharesarn)
    for share in shares['NFSFileShareInfoList']:
        location_arn = share['LocationARN']
        m = location_arn_re.match(location_arn)
        if m and m.group(1) == bucket_name:
            http_response['statusConde'] = 200
            http_response['body'] = http_response['body'] + "Refreshing " + location_arn + "\n"
            cli.refresh_cache(FileShareARN = share['FileShareARN'])
    return http_response

def refresh_share(bucket_name):
    cli = boto3.client('storagegateway', region_name='us-east-1')

    response = cli.list_file_shares()
    filesharesarn = []
    for fileshare in response['FileShareInfoList']:
        print("FileShareId: %s" % fileshare['FileShareId'])
        filesharesarn.append(fileshare['FileShareARN'])

    shares = cli.describe_nfs_file_shares(FileShareARNList=filesharesarn)
    for share in shares['NFSFileShareInfoList']:
        print(share)

if __name__ == '__main__':
    response = process({"bucket_name": "cci-dev-us-east-1-file-gateway-test"})
    print(response)
