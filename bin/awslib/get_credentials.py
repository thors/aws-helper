import os
import re
import logging
import gnupg
from awslib.const import *
import awslib.read_config


log = logging.getLogger("get_credentials")


def _read_encrypted_credentials_file(file_path):
    if not os.path.exists(AWS_CREDENTIALS_FILE_PATH_ENCRYPTED):
        log.error("File with encrypted credentials (%s) not found", AWS_CREDENTIALS_FILE_PATH_ENCRYPTED)
    gpg_config = awslib.read_config.read_config_file(GPG_CONFIG_FILE)
    user = gpg_config.get("user", GPG_DEFAULT_USER)
    gpg = gnupg.GPG()
    with open(AWS_CREDENTIALS_FILE_PATH_ENCRYPTED, "rb") as encrypted_credentials_file:
        data_enc = encrypted_credentials_file.read()
        data = gpg.decrypt(data_enc)
        return data.data.decode("utf8").split("\n")


def _read_credentials_file():
    if os.path.exists(AWS_CREDENTIALS_FILE_PATH):
        log.warning("You have an unencrypted credentials file; consider encrypting it "
                    "following advise in doc/security.md")
        with open(AWS_CREDENTIALS_FILE_PATH, "r", encoding="utf8") as credentials_file:
            return credentials_file.read().split("\n")
    else:
        return _read_encrypted_credentials_file(AWS_CREDENTIALS_FILE_PATH_ENCRYPTED)


def get_aws_credentials():
    accounts = {}
    account = ""
    accounts[account] = {'aws_access_key_id': '', 'aws_secret_access_key': ''}
    reAccount = re.compile("\[(.*)\]")
    reAccessKey = re.compile("aws_access_key_id=(.*)")
    reSecretAccessKey = re.compile("aws_secret_access_key=(.*)")

    credentials = _read_credentials_file()
    for line in credentials:
        m = reAccount.match(line)
        if m:
            account = m.group(1)
            accounts[account] = {'aws_access_key_id': '', 'aws_secret_access_key': ''}
            continue
        m = reAccessKey.match(line)
        if m:
            accounts[account]['aws_access_key_id'] = m.group(1)
            continue
        m = reSecretAccessKey.match(line)
        if m:
            accounts[account]['aws_secret_access_key'] = m.group(1)
    if not accounts['']['aws_access_key_id']:
        accounts.pop('', None)
    return accounts
