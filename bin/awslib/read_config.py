import os
import re
import logging


def read_config_file(file_name):
    log = logging.getLogger("read_config")
    result = {}
    if not os.path.exists(file_name):
        log.warning("File %s does not exist, returning empty array", file_name)
        return result
    with open(file_name, "r", encoding="utf8") as config_file:
        valid_line_re = re.compile("^([a-zA-Z0-9_]*)=(.*)$")
        for line in config_file:
            line = line.strip("\n")
            m = valid_line_re.match(line)
            if m:
                result[m.group(1)] = m.group(2)
        return result
