#from bson import json_util
import os
import datetime
import json
import boto3
import botocore
from awslib.get_credentials import *


""" 
We assume batch operation, not interactive. therefore we cache as much as possible within this library, but we are not reading from disk, always from AWS to make sure we are not outdated. 
"""


def default_json_serializer(o):
    if isinstance(o, (datetime.date, datetime.datetime)):
        return o.isoformat()


class GetInfo:
    def __init__(self):
        self.credentials = get_aws_credentials()
        home = expanduser("~")
        self.datapathbase = os.path.join(home, ".aws", "aws-helper")
        if not os.path.exists(os.path.join(self.datapathbase, "summary")):
            os.makedirs(os.path.join(self.datapathbase, "summary"))
        self.regions = {}
        services = ['ec2', 'sts', 'organizations', 's3', 'cloudwatch', 'iam', 'ecr']
        self.clients = {}
        for service in services:
            self.clients[service] = {}

    def get_client(self, service: str, account: str, region: str):
        if service not in self.clients.keys():
            print("ERROR: Service %s not supported" % service)
            exit(1)
        if account not in self.clients[service].keys():
            self.clients[service][account] = {}
        if region not in self.clients[service][account].keys():
            pass
        self.clients[service][account][region] = boto3.client(service, aws_access_key_id=self.credentials[account]['aws_access_key_id'],
                                                              aws_secret_access_key=self.credentials[account]['aws_secret_access_key'],
                                                              region_name=region)
        return self.clients[service][account][region]

    def get_path(self, account=None):
        path = None
        if account:
            account_id = self.account_number(account)
            path = os.path.join(self.datapathbase, "account", account_id)
        else:
            path = os.path.join(self.datapathbase, "summary")
        if not os.path.exists(path):
            os.makedirs(path)
        return path

    def describe_regions(self, account):
        if account not in self.regions.keys():
            self.regions[account] = self.get_client('ec2', account, 'us-east-1').describe_regions()['Regions']
        return self.regions[account]

    def account_number(self, account):
        if account not in self.credentials.keys():
            raise Exception("Account %s not in ~/.aws/credentials" % account)
        if 'account_id' not in self.credentials[account].keys():
            sts_client = self.get_client('sts', account, 'us-east-1')
            account_id = sts_client.get_caller_identity().get('Account')
            self.credentials[account]['account_id'] = account_id
        return self.credentials[account]['account_id']

    def get_clean_credentials(self, p_account=None):
        if p_account:
            accounts = [p_account]
        else:
            accounts = self.credentials.keys()
        
        with open(os.path.join(self.datapathbase, "credentials.clean"), 'w') as credentials_file:
            for account in accounts:
                sts_client = self.get_client('sts', account, 'us-east-1')
                orga_client = self.get_client('organizations', account, 'us-east-1')
                try:
                    account_id = sts_client.get_caller_identity().get('Account')
                    account_name = orga_client.describe_account(AccountId=account_id).get('Account').get('Name')
                except orga_client.exceptions.AccessDeniedException as e:
                    credentials_file.write("# Can't get official account name, access to client 'organizations' denied\n")
                    account_name = account
                except botocore.exceptions.ClientError as e:
                    print("Invalid credentials for account %s" % (account))
                    credentials_file.write("#[%s]\n#Invalid credentials!\n#aws_access_key_id=%s\na#ws_secret_access_key=%s\n\n" %
                                       (account,
                                        self.credentials[account]['aws_access_key_id'],
                                        self.credentials[account]['aws_secret_access_key']))
                    continue

                credentials_file.write("[%s]\n#account_id=%s\naws_access_key_id=%s\naws_secret_access_key=%s\n\n" %
                                       (account_name,
                                        account_id,
                                        self.credentials[account]['aws_access_key_id'],
                                        self.credentials[account]['aws_secret_access_key']))
            
    def get_vpc_info(self, p_account = None):
        if p_account:
            accounts = [p_account]
        else:
            accounts = self.credentials.keys()

        for account in accounts:
            print("account:" + account)
            try:
                regions = self.describe_regions(account)
            except botocore.exceptions.ClientError as e:
                print("ERROR: Credentials outdated for account %s. Skipping." % account)
                continue
            for region_obj in regions:
                region = region_obj['RegionName']
                ec2client = self.get_client('ec2', account, region)
                vpcmap = ec2client.describe_vpcs()
                if len(vpcmap['Vpcs']) == 0:
                    continue
                with open(os.path.join(self.get_path(account), "vpcs_" + region + ".json"), "w") as vpcs_file:
                    j = json.dumps(vpcmap)
                    vpcs_file.write(j)
                with open(os.path.join(self.get_path(account), "vpcs_" + region + ".csv"), "w") as vpcs_file:
                    vpcs_file.write("account-alias,account,region,vpc-id,cidr-block,name\n")
                    for vpc in vpcmap['Vpcs']:
                        name = ""
                        if vpc['CidrBlock'] == "172.31.0.0/16":
                            # Ignore. This is provided by AWS for each account and should not be seperately documented
                            continue
                        if 'Tags' in vpc.keys():
                            tags = vpc[u'Tags']
                            for tag in tags:
                                if tag[u'Key'] == 'Name':
                                    name = tag[u'Value']
                                    break
                        vpcs_file.write("%s,%s,%s,%s,%s,%s\n" % 
                                        (account,
                                         self.account_number(account),
                                         region,
                                         vpc['VpcId'],
                                         vpc['CidrBlock'],
                                         name))

    def get_subnet_info(self,  p_account = None):
        if p_account:
            accounts = [p_account]
        else:
            accounts = self.credentials.keys()

        for account in accounts:
            print("account:" + account)
            try:
                regions = self.describe_regions(account)
            except botocore.exceptions.ClientError as e:
                print("ERROR: Credentials outdated for account %s. Skipping." % account)
                continue
            for region_obj in regions:
                region = region_obj['RegionName']
                ec2_client = self.get_client('ec2', account, region)
                account_id = self.account_number(account)
                response = ec2_client.describe_subnets()
                subnets = response[u'Subnets']
                with open(os.path.join(self.get_path(account), "subnets_%s.json" % (region)), "w") as opf:
                    j = json.dumps(subnets, indent=2)
                    opf.write(j)
                with open(os.path.join(self.get_path(account), "subnets_%s.csv" % (region)), "w") as opf:
                    opf.write("account-alias,account,subnet-id,vpc-id,availability-zone,cidr-block,name\n")
                    for subnet in subnets:
                        name = ''
                        if 'Tags' in subnet.keys():
                            tags = subnet[u'Tags']
                            for tag in tags:
                                if tag[u'Key'] == 'Name':
                                    name = tag[u'Value']
                                    break

                        opf.write("{0},{1},{2},{3},{4},{5},{6}\n".format(account,
                                                                         account_id,
                                                                         subnet[u'SubnetId'],
                                                                         subnet[u'VpcId'],
                                                                         subnet[u'AvailabilityZone'],
                                                                         subnet[u'CidrBlock'],
                                                                         name))

    def get_az_mapping_info(self,  p_account=None):
        if p_account:
            accounts = [p_account]
        else:
            accounts = self.credentials.keys()

        for account in accounts:
            try:
                regions  = self.describe_regions(account)
                for region_obj in regions:
                    region = region_obj['RegionName']
                    ec2_client = self.get_client('ec2', account, region)
                    account_id = self.account_number(account)
                    response = ec2_client.describe_availability_zones()
                    with open(os.path.join(self.get_path(account), "az_mapping_%s.csv" % (region)), "w") as opf:
                        opf.write("account-alias,account,region,availability-zone,availability-zone-id,network-border-group\n")
                        for zone in response['AvailabilityZones']:
                            opf.write("{0},{1},{2},{3},{4},{5}\n".format(account,
                                                                         account_id,
                                                                         zone.get('RegionName', ""),
                                                                         zone['ZoneName'],
                                                                         zone['ZoneId'],
                                                                         zone.get("NetworkBorderGroup", "")))
            except Exception as e:
                print("Account: %s, Failure: %s" % (account, str(e)))

    def get_access_keys(self,  p_account=None):
        if p_account:
            accounts = [p_account]
        else:
            accounts = self.credentials.keys()

        keys = {}
        for account in accounts:
            try:
                iam_client = self.get_client('iam', account, 'us-east-1')
                account_id = self.account_number(account)
                resp = iam_client.list_access_keys()
                keys[account_id] = resp['AccessKeyMetadata']
            except Exception as e:
                print("Account: %s, Failure: %s" % (account, str(e)))
        return keys

    def get_ecr_info(self,  p_account=None):
        if p_account:
            accounts = [p_account]
        else:
            accounts = self.credentials.keys()
        for account in accounts:
            try:
                regions = self.describe_regions(account)
            except botocore.exceptions.ClientError as e:
                print("ERROR: Credentials outdated for account %s. Skipping." % account)
                continue

            for region_obj in regions:
                region = region_obj['RegionName']
                print("%s: %s" % (account, region))
                cli = self.get_client('ecr', account, region)
                filename = os.path.join(self.get_path(account), "ecr_images_%s.csv" % region)
                print("Writing to file: %s" % filename)
                content_found = False
                with open(filename, "w") as opf:
                    opf.write("Account,Region,Image_base_name,Tag\n")
                    page_repos = 1
                    while page_repos:
                        if page_repos == 1:
                            repos =  cli.describe_repositories()
                        else:
                            repos =  cli.describe_repositories(nextToken=page_repos)

                        for item in repos['repositories']:
                            repo_name = item['repositoryName']
                            page_images = 1
                            images = None
                            while page_images:
                                if page_images == 1:
                                    images = cli.list_images(
                                        repositoryName=repo_name,
                                        filter={
                                            'tagStatus': 'TAGGED'
                                        }
                                    )
                                else:
                                    images = cli.list_images(
                                            repositoryName=repo_name,
                                            filter={
                                                'tagStatus': 'TAGGED',
                                            },
                                           nextToken=page_images
                                        )
                                if images:
                                    for image_id in images['imageIds']:
                                        content_found = True
                                        line = "%s,%s,%s,%s\n" % (account,region,repo_name, image_id['imageTag'])
                                        opf.write(line)
                                page_images = images.get('nextToken', None)
                        page_repos = repos.get('nextToken', None)
                    if not content_found:
                        os.remove(filename)

    def get_s3_bucket_info(self,  p_account=None):
        if p_account:
            accounts = [p_account]
        else:
            accounts = self.credentials.keys()

        for account in accounts:
            s3_client = self.get_client('s3', account, 'us-east-1')
            account_id = self.account_number(account)
            response = s3_client.list_buckets()
            buckets = response[u'Buckets']
            with open(os.path.join(self.get_path(account), "s3_buckets.json"), "w") as opf:
                j = json.dumps(buckets, indent=2, default=default_json_serializer)
                opf.write(j)
            with open(os.path.join(self.get_path(account), "s3_buckets.csv"), "w") as opf:
                opf.write("account-alias,account,region,name,creation_date,size_std,size_ia,size_total,newest_object,newest_obj_date\n")
                for bucket in buckets:
                    name = bucket['Name']
                    region = s3_client.get_bucket_location(Bucket=name)['LocationConstraint']
                    if not region:
                        region = 'us-east-1'
                    cloudwatch_client = self.get_client('cloudwatch', account, region)
                    creation_date = str(bucket['CreationDate'])
                    response = cloudwatch_client.get_metric_statistics(
                        Namespace="AWS/S3",
                        MetricName="BucketSizeBytes",
                        Dimensions=[
                            {
                                "Name": "BucketName",
                                "Value": name
                            },
                            {
                                "Name": "StorageType",
                                "Value": "StandardStorage"
                            }
                        ],
                        StartTime = datetime.datetime.now() - datetime.timedelta(days=7),
                        EndTime=datetime.datetime.now(),
                        Period = 86400,
                        Statistics=['Average']
                    )
                    if response['Datapoints']:
                        bucket_size_bytes_std = response['Datapoints'][-1]['Average']
                    else:
                        bucket_size_bytes_std = -1

                    response = cloudwatch_client.get_metric_statistics(
                        Namespace="AWS/S3",
                        MetricName="BucketSizeBytes",
                        Dimensions=[
                            {
                                "Name": "BucketName",
                                "Value": name
                            },
                            {
                                "Name": "StorageType",
                                "Value": "StandardIAStorage"
                            }
                        ],
                        StartTime = datetime.datetime.now() - datetime.timedelta(days=7),
                        EndTime=datetime.datetime.now(),
                        Period = 86400,
                        Statistics=['Average']
                    )

                    if response['Datapoints']:
                        bucket_size_bytes_ia = response['Datapoints'][-1]['Average']
                    else:
                        bucket_size_bytes_ia = -1

                    opf.write("{0},{1},{2},{3},{4},{5},{6},{7}\n".format(account,
                                                                     account_id,
                                                                     region,
                                                                     name,
                                                                     creation_date,
                                                                     bucket_size_bytes_std,
                                                                     bucket_size_bytes_ia,
                                                                     bucket_size_bytes_std + bucket_size_bytes_ia
                                                                     ))

    def summarize(self, topic):
        with open(os.path.join(self.get_path(), "{0}.csv".format(topic)), "w") as summary_file:
            headers_printed = False
            for (root, folders, files) in os.walk(os.path.join(self.datapathbase, "account")):
                for f in files:
                    if f[0:len(topic)] == topic and f[-4:] == ".csv":
                        with open(os.path.join(root,f), 'r') as input_file:
                            first_line = input_file.readline()
                            if not headers_printed:
                                summary_file.write(first_line)
                                headers_printed = True
                            for line in input_file:
                                summary_file.write(line)
