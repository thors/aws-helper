import os

AWS_CREDENTIALS_FILE_PATH = os.path.join(os.path.expanduser("~"), ".aws", "credentials")
AWS_CREDENTIALS_FILE_PATH_ENCRYPTED = AWS_CREDENTIALS_FILE_PATH + ".gpg"
GPG_CONFIG_FILE = os.path.join(os.path.expanduser("~"), ".aws", "gpg")
GPG_DEFAULT_USER = "aws-helper@localhost"
